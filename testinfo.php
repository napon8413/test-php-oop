<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body >
    

    <table width="100%">
        <thead>
            <tr>
                <td>PlandetailtID</td>
                <td>PlantBanefity</td>
                <td>PlantBanefityEng</td>
                <td>PlantBenefit</td>
                <td>PlantBenefitEng</td>
                <td>PlantCommonname</td>
                <td>PlantDiscover</td>
                <td>PlantDistrbution</td>
                <td>PlantDistrbutionEng</td>
                <td>PlantFlower</td>
                <td>PlantFlowerEng</td>
                <td>PlantID</td>
                <td>PlantLeaf</td>
                <td>PlantLeafEng</td>
                <td>PlantName</td>
                <td>PlantRound/td>
                <td>PlantRoundEng</td>
                <td>PlantScience</td>
                <td>PlantSeed</td>
                <td>PlantSeedEng</td>
                <td>PlantStem</td>
                <td>PlantStemEng</td>
                <td>PlantType</td>
                <td>PlantTypeENG</td>
                <td>PlantfamilyID</td>
                <td>SeasonID</td>
            </tr>
        </thead>

        <tbody id="info">

        </tbody>

    </table>

    <button type="button" onclick="loadDoc()">Change content</button>


    <script>
        function loadDoc(){
            let xhttp= new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if (this.readyState == 4 && this.status == 200){
                    let data = JSON.parse(this.responseText);
                    console.log(data);

                    for (let i = 0; i < data.length; i++){
                        document.getElementById('info').innerHTML +=`
                            <tr>
                                <td>${data[i].PlandetailtID}</td>
                                <td>${data[i].PlantBanefity}</td>
                                <td>${data[i].PlantBanefityEng}</td>
                                <td>${data[i].PlantBenefit}</td>
                                <td>${data[i].PlantBenefitEng}</td>
                                <td>${data[i].PlantCommonname}</td>
                                <td>${data[i].PlantDiscover}</td>
                                <td>${data[i].PlantDistrbution}</td>
                                <td>${data[i].PlantDistrbutionEng}</td>
                                <td>${data[i].PlantFlower}</td>
                                <td>${data[i].PlantFlowerEng}</td>
                                <td>${data[i].PlantID}</td>
                                <td>${data[i].PlantLeaf}</td>
                                <td>${data[i].PlantLeafEng}</td>
                                <td>${data[i].PlantName}</td>
                                <td>${data[i].PlantRound}</td>
                                <td>${data[i].PlantRoundEng}</td>
                                <td>${data[i].PlantScience}</td>
                                <td>${data[i].PlantSeed}</td>
                                <td>${data[i].PlantSeedEng}</td>
                                <td>${data[i].PlantStem}</td>
                                <td>${data[i].PlantStemEng}</td>
                                <td>${data[i].PlantType}</td>
                                <td>${data[i].PlantTypeENG}</td>
                                <td>${data[i].PlantfamilyID}</td>
                                <td>${data[i].SeasonID}</td>
                            </tr>
                        `;
                    }

                }
            }
            xhttp.open("GET", 'index.php', true)
            xhttp.send();
        }
    </script>

</body>
</html>